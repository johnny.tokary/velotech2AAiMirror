import gradio as gr
import numpy as np
import torch
import cv2
from ultralytics import YOLO
from PIL import Image
import os

import logging
logging.basicConfig(level=logging.DEBUG)

# loaded_model = load_model(os.path.join('.','models', 'light_onoff_model.h5'))
modelC = torch.load("models/LightDetectionCNN.pt")
modelF = YOLO("models/YOLOv8.pt")


def pipelineEndToEnd(video):
    gr.Info("pulling image frames from video")
    result_gallery = []  # Een lijst om de uitgeknipte lantaarnpalen op te slaan
    track_history:dict = {}
    # Simulatie van een detectiemodel (vervang dit met je eigen detectiemodel)
    logging.debug(video)
    # Lees de video in
    cap = cv2.VideoCapture(video)
    
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        
        # Detecteer lantaarns in het huidige frame
        results = modelF.track(frame, persist=True)
        
        frame_ = results[0].plot()
        if(results[0].boxes.id!=None):
            boxes = results[0].boxes
            track_ids = results[0].boxes.id.int().cpu().tolist()
            for box, track_id in zip(boxes, track_ids):
                logging.debug(track_id)
                logging.debug(float(box.conf[0]))
                logging.debug(box.xyxy[0])
                x_min, y_min, x_max, y_max = box.xyxy[0].int()
                if(track_history.__contains__(track_id)):
                    if track_history[track_id]["conf"] < float(box.conf[0]):
                        track_history[track_id] = {"conf": float(box.conf[0]), 
                                                    "img": frame[y_min:y_max, x_min:x_max]}
                else:
                
                    track_history[track_id] = {"conf": float(box.conf[0]), 
                                            "img": frame[y_min:y_max, x_min:x_max]}
            keys = list(track_history.keys())
            for key in keys:
                if not track_ids.__contains__(key):
                    highest = track_history.pop(key)
                    im = Image.fromarray(highest["img"])
                    result_gallery.append((im,f"id:{str(key)} conf:{round(highest['conf'],2)} "))
    keys = list(track_history.keys())
    for key in keys:
        highest = track_history.pop(key)
        im = Image.fromarray(highest["img"])
        result_gallery.append((im,f"id:{str(key)} conf:{round(highest['conf'],2)} "))       
    # Sluit de video stream
    cap.release()
    gr.Info("checking the images with CNN")
    
    res = []
    for galeryItem in result_gallery:
        img  = galeryItem[0]
        result = "off"
        img = np.array(img)    
        resize = cv2.resize(img, (256, 256))
        resize = np.expand_dims(resize/255, 0)
        resize = resize.astype(np.float32)
        resize = torch.from_numpy(resize)
        print(resize)
        with torch.no_grad():
            modelC.eval()
            output = modelC(resize)

        if output.item() > 0.5:
            result = "on"
        
        res.append((galeryItem[0],galeryItem[1] + f"state:{result}"))
        
    
    return res

def video_tracking(video):
    result = "out.mp4"
    cap = cv2.VideoCapture(video)
    ret, frame = cap.read()
    
    H, W, _ = frame.shape
    out = cv2.VideoWriter(result, cv2.VideoWriter_fourcc(*'avc1'), int(cap.get(cv2.CAP_PROP_FPS)), (W, H))
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        results = modelF.track(frame, persist=True)
        frame_ = results[0].plot()
        out.write(frame_)
        
    cap.release()
    out.release()
    
    return result

def detect_and_crop_lanterns(video):
    gr.Info("pulling image frames from video")
    result_gallery = []  # Een lijst om de uitgeknipte lantaarnpalen op te slaan
    track_history:dict = {}
    # Simulatie van een detectiemodel (vervang dit met je eigen detectiemodel)
    logging.debug(video)
    # Lees de video in
    cap = cv2.VideoCapture(video)
    
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        
        # Detecteer lantaarns in het huidige frame
        results = modelF.track(frame, persist=True)
        
        frame_ = results[0].plot()
        if(results[0].boxes.id!=None):
            boxes = results[0].boxes
            track_ids = results[0].boxes.id.int().cpu().tolist()
            for box, track_id in zip(boxes, track_ids):
                logging.debug(track_id)
                logging.debug(float(box.conf[0]))
                logging.debug(box.xyxy[0])
                x_min, y_min, x_max, y_max = box.xyxy[0].int()
                if(track_history.__contains__(track_id)):
                    if track_history[track_id]["conf"] < float(box.conf[0]):
                        track_history[track_id] = {"conf": float(box.conf[0]), 
                                                    "img": frame[y_min:y_max, x_min:x_max]}
                else:
                
                    track_history[track_id] = {"conf": float(box.conf[0]), 
                                            "img": frame[y_min:y_max, x_min:x_max]}
            keys = list(track_history.keys())
            for key in keys:
                if not track_ids.__contains__(key):
                    highest = track_history.pop(key)
                    im = Image.fromarray(highest["img"])
                    result_gallery.append((im,f"id:{str(key)} conf:{round(highest['conf'],2)} "))
    keys = list(track_history.keys())
    for key in keys:
        highest = track_history.pop(key)
        im = Image.fromarray(highest["img"])
        result_gallery.append((im,f"id:{str(key)} conf:{round(highest['conf'],2)} "))       
    # Sluit de video stream
    cap.release()
    
    
    return result_gallery

def imageClassification(img):
    gr.Info("checking the image with CNN")
    result = "off"
    img = np.array(img)    
    resize = cv2.resize(img, (256, 256))
    resize = np.expand_dims(resize/255, 0)
    resize = resize.astype(np.float32)
    resize = torch.from_numpy(resize)
    print(resize)
    with torch.no_grad():
        modelC.eval()
        output = modelC(resize)

    if output.item() > 0.5:
        result = "on"
    
    return result


YOLOV8 = gr.Interface(detect_and_crop_lanterns,
                    gr.Video(), 
                    gr.Gallery())

YOLOV8Vid = gr.Interface(video_tracking,
                    gr.Video(), 
                    "playablevideo")

ImgCNN = gr.Interface(
    imageClassification,
    gr.Image(),
    gr.Label(),
)

EndToEnd = gr.Interface(
    pipelineEndToEnd,
    gr.Video(),
    gr.Gallery(),
)



demo = gr.TabbedInterface([YOLOV8Vid, YOLOV8, ImgCNN, EndToEnd],["YOLO v8 Tracking","YOLO v8 detect_and_crop_lanterns","CNN ImageClassification", "End to End"])


if __name__ == "__main__":
    demo.launch(server_name="0.0.0.0", server_port=8080)
