#https://docs.opencv.org/4.x/d2/de6/tutorial_py_setup_in_ubuntu.html
git clone https://github.com/opencv/opencv.git
cd opencv

mkdir build
cd build

cmake ../

make -j 8
make install

cd ~ rm -rf opencv

# could try https://github.com/opencv/opencv-python#manual-builds in sh file if this not works